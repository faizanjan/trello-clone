import React, { useState, useRef, useEffect } from "react";
import { useErrorUpdate } from "../contexts/ErrorContext";
import { createItem } from "../modules/TrelloAPI";

const AddNew = ({ parentId, parentName, itemName, onItemAdded }) => {
  const [showForm, setShowForm] = useState(false);
  const [newItem, setNewItem] = useState("");
  const setError = useErrorUpdate();

  const inputRef = useRef();
  useEffect(() => {
    if (showForm) {
      inputRef.current.focus();
    }
  }, [showForm]);

  const addItem = (e) => {
    e.preventDefault();
    if (newItem.trim() === "") {
      alert("Field can't be empty");
      return;
    }
    createItem(itemName, newItem, parentName, parentId)
      .then((res) => {
        setNewItem("");
        onItemAdded(res.data);
      })
      .catch((error) => {
        setError(error.message);
      });
  };

  const toggleForm = () => {
    setShowForm(!showForm);
  };

  return (
    <div className={"add-new-container"}>
      <div className="custom-details">
        <div
          className={
            "summary my-2 " + (itemName === "Board" ? "fs-4 me-5" : "")
          }
          onClick={toggleForm}
        >
          <span>
            <i
              className={
                (showForm ? "fa-solid fa-minus " : "fa-regular fa-plus ") +
                (itemName === "Board" ? "" : "ms-4 mt-4 mb-3")
              }
            ></i>{" "}
            &nbsp; Add a {itemName}
          </span>
        </div>
        <form
          onSubmit={addItem}
          className="mx-auto border-top add-new"
          style={{
            width: "100%",
            height: `${showForm ? "inherit" : "0"}`,
            display: `${showForm ? "inherit" : "none"}`,
          }}
        >
          <input
            type="text"
            ref={inputRef}
            className="border shadow mt-3 mb-1 mx-auto p-3 rounded-3 bg-light text-dark"
            placeholder={"New " + itemName + " Name"}
            value={newItem}
            onChange={(e) => setNewItem(e.target.value)}
            style={{ width: "90%" }}
          />
          <button className="btn btn-outline bg-light mb-3" type="submit">
            {"Add " + itemName}
          </button>
        </form>
      </div>
    </div>
  );
};

export default AddNew;
