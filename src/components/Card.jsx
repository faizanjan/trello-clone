import Card from "react-bootstrap/Card";
import { useState } from "react";
import CardModal from "./CardModal";
import { deleteItem } from "../modules/TrelloAPI";
import { useErrorUpdate } from "../contexts/ErrorContext";

let ListCard = ({ card, onCardDelete, listname }) => {
  let [showDeleteBtn, setDeleteBtn] = useState(false);
  const [modalShow, setModalShow] = useState(false);
  const setError = useErrorUpdate();

  let deleteCard = () => {
    deleteItem("card", card.id)
      .then(() => {
        onCardDelete();
      })
      .catch((error) => {
        setError(error.message);
      });
  };

  return (
    <Card
      border="none"
      className="rounded-3 shadow"
      style={{ width: "95%", margin: "2px auto", color: "rgb(51,68,98)" }}
      onMouseOver={() => {
        if (!modalShow) setDeleteBtn(true);
      }}
      onMouseOut={() => setDeleteBtn(false)}
      onClickCapture={(e) => {
        if (!e.target.matches("i")) setModalShow(true);
      }}
    >
      <Card.Body className="d-flex flex-column justify-content-center align-items-start list-card-hover">
        <div className="d-flex flex-row justify-content-between align-items-center w-100">
          <span>{card.name} </span>
          {showDeleteBtn && (
            <i className="fa-solid fa-trash-can" onClick={deleteCard}></i>
          )}
        </div>
        {card.badges.checkItems !== 0 && (
          <div
            className="checkitem-in-card mt-2 opacity-75"
            style={{ fontSize: "0.5rem" }}
          >
            <i className="fa-solid fa-list-check me-3"></i>
            {card.badges.checkItemsChecked + "/" + card.badges.checkItems}
          </div>
        )}
      </Card.Body>

      {/* CARD MODAL */}
      <CardModal
        show={modalShow}
        onHide={() => {
          setModalShow(false);
        }}
        card={card}
        listname={listname}
      />
    </Card>
  );
};

export default ListCard;
