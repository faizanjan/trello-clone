import { Dropdown } from "react-bootstrap";
import ListCard from "./Card";
import { useState, useEffect, createContext } from "react";
import { useErrorUpdate } from "../contexts/ErrorContext";
import AddNew from "./AddNewItem";
import { getAllChildren, deleteList } from "../modules/TrelloAPI";

export const UpdateListContext = createContext();

let List = ({ list, onListArchived }) => {
  const [isLoading, setIsLoading] = useState(true);
  const [allCards, setAllCards] = useState(null);
  const [areCardsUpdated, setAreCardsUpdated] = useState(false);

  const setError = useErrorUpdate();
  useEffect(() => {
    getAllChildren("list", "card", list.id)
      .then((data) => {
        setAllCards(data.data);
        setIsLoading(false);
        setAreCardsUpdated(false);
      })
      .catch((error) => {
        setError(error.message);
        setIsLoading(false);
      });
  }, [areCardsUpdated]);

  const addNewCard = (card)=>{
    setAllCards((prevState)=>[...prevState, card])
  }

  let onItemRemoved = () => {
    setAreCardsUpdated(true);
  };

  let archiveList = () => {
    deleteList(list)
      .then((res) => {
        onListArchived();
      })
      .catch((error) => {
        console.error(error.response);
      });
  };

  if (isLoading) return;

  return (
    <span
      className="List d-inline-block m-2 rounded-4 px-2 pt-3 shadow"
      style={{ color: "rgb(51,68,98)" }}
    >
      <header className="d-flex flex-row justify-content-between mb-3 bg-light rounded-3 py-1">
        <span className="heading p-2 ps-4 fw-bold">{list.name}</span>
        <Dropdown>
          <Dropdown.Toggle variant="light" id="ellipsis-menu"></Dropdown.Toggle>

          <Dropdown.Menu>
            <Dropdown.Item onClick={archiveList}>Delete List</Dropdown.Item>
            <Dropdown.Item disabled onClick={() => {}}>
              Rename List
            </Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      </header>

      <div className="cards-list">
        {allCards.map((card) => (
          <UpdateListContext.Provider key={card.id} value={setAreCardsUpdated}>
            <ListCard
              card={card}
              listname={list.name}
              onCardDelete={onItemRemoved}
            />
          </UpdateListContext.Provider>
        ))}
        <AddNew
          parentId={list.id}
          parentName="List"
          itemName="Card"
          onItemAdded={addNewCard}
        />
      </div>
    </span>
  );
};

export default List;
