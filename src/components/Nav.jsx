import Container from "react-bootstrap/Container";
import { Link } from "react-router-dom";
import Navbar from "react-bootstrap/Navbar";

function NavigationBar() {
  return (
    <Navbar
      collapseOnSelect
      expand="lg"
      className="border-bottom-secondary d-flex align-items-center py-3 border-light border-bottom"
    >
      <Container>
        <Link to="/boards">
          <Navbar.Brand className="logo py-3 px-2 rounded-3 shadow">
            <img
              className="w-100 mx-auto"
              src="https://upload.wikimedia.org/wikipedia/en/archive/8/8c/20210216184933%21Trello_logo.svg"
              alt="logo"
              style={{ height: "30px" }}
            />
          </Navbar.Brand>
        </Link>
        <input
          type="text"
          placeholder="Search"
          className="ps-3 p-2 rounded border-light"
        />
      </Container>
    </Navbar>
  );
}

export default NavigationBar;
