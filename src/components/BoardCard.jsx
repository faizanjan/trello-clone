import { useState } from "react";
import Card from "react-bootstrap/Card";
import { Link } from "react-router-dom";

function BoardCard({ board }) {
  let { name, prefs, desc, starred } = board;
  let [showFavBtn, setShowFavBtn] = useState(false);

  let toggleFav = () => {
    // axios.put()
  };

  return (
    <Link to={"/boards/" + board.id}>
      <Card
        style={{
          backgroundImage: `linear-gradient(RGB(145, 147, 150, 0.3 ), RGB(0,0,0,0.3)), ${prefs.backgroundImage!==null? `URL(${prefs.backgroundImage})`: prefs.background}`,
        }}
        className="mb-5 mx-3 board-card"
        onMouseOver={() => {
          setShowFavBtn(true);
        }}
        onMouseOut={() => {
          setShowFavBtn(false);
        }}
      >
        <Card.Header>{name}</Card.Header>
        <Card.Body className="board-card-desc">
          <p>{desc}</p>
        </Card.Body>
        {starred && (
          <i
            id="board-card-star"
            className="fa-solid fa-star"
            onClick={() => {
              toggleFav(false);
            }}
          ></i>
        )}
        {!starred && showFavBtn && (
          <i
            id="board-card-star"
            className="fa-regular fa-star"
            onClick={() => {
              toggleFav(true);
            }}
          ></i>
        )}
      </Card>
    </Link>
  );
}

export default BoardCard;
