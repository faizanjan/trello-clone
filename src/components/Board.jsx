import List from "./List";
import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { useErrorUpdate } from "../contexts/ErrorContext";
import { getAllChildren, getBoardDetails } from "../modules/TrelloAPI";
import Loader from "./Loader";
import AddNew from "./AddNewItem";

let Board = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [allLists, setAllLists] = useState(null);
  const [boardDetails, setBoardDetails] = useState(null);
  const [listsUpdated, setListsUpdated] = useState(false);
  const setError = useErrorUpdate();
  const { boardId } = useParams();

  useEffect(() => {
    getAllChildren("board", "list", boardId)
      .then((data) => {
        setAllLists(data.data);
        setIsLoading(false);
        setListsUpdated(false);
      })
      .catch((error) => {
        setError(error.message);
        setIsLoading(false);
      });

    getBoardDetails(boardId)
      .then((res) => {
        setBoardDetails(res.data);
      })
      .catch((error) => {
        console.error("Error fetching country data:", error);
        setIsLoading(false);
      });
  }, [listsUpdated]);

  const addNewList = (list) => {
    setAllLists((prevState) => [...prevState, list]);
  };

  let onItemRemoved = () => {
    setListsUpdated(true);
  };

  if (isLoading) {
    return (
      <div>
        <Loader />
      </div>
    );
  }

  return (
    <div
      className="board-page vw-100 d-flex flex-column align-items-start"
      style={{
        backgroundImage: `${
          boardDetails
            ? "url(" + boardDetails.prefs.backgroundImage + ")"
            : "inherit"
        }`,
      }}
    >
      <h3
        className="text-dark position-fixed p-3 ps-5"
        style={{
          background: "rgb(255,255,255,0.4)",
          margin: 0,
          width: "100vw",
        }}
      >
        {boardDetails?.name || ""}
      </h3>

      <div className="lists-container d-flex flex-row align-items-start mt-5 py-4 mx-3">
        {allLists.map((list) => (
          <List key={list.id} list={list} onListArchived={onItemRemoved} />
        ))}
        <div className="add-list-head List d-inline-block m-2 rounded-4 p-2 fs-5 shadow">
          <AddNew
            parentId={boardDetails?.id}
            parentName="Board"
            itemName="List"
            onItemAdded={addNewList}
          />
        </div>
      </div>
    </div>
  );
};

export default Board;
