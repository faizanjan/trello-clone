import { useState, useEffect } from "react";
import BoardCard from "./BoardCard";
import Loader from "./Loader";
import AddNew from "./AddNewItem";
import Card from "react-bootstrap/Card";
import { getAllBoards } from "../modules/TrelloAPI";
import { useErrorUpdate } from "../contexts/ErrorContext";

let Home = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [allBoards, setAllBoards] = useState(null);

  const setError = useErrorUpdate();

  useEffect(() => {
    getAllBoards()
      .then((data) => {
        setAllBoards(data.data);
        setIsLoading(false);
      })
      .catch((error) => {
        setError(error.message);
        setIsLoading(false);
      });
  }, []);

  const addNewBoard = (board) => {
    setAllBoards((prevState) => [...prevState, board]);
  };

  if (isLoading) {
    return (
      <div>
        <Loader />
      </div>
    );
  }

  return (
    <div className="w-75 d-flex flex-column mx-auto mt-5 flex-wrap">
      <h3 className=" text-light mb-4">
        {" "}
        <i className="fa-regular fa-star"></i> &nbsp; Starred Boards
      </h3>
      <div className="starred-boards d-flex flex-row mt-2 mb-4 flex-wrap justify-content-start">
        {allBoards
          .filter((board) => board.starred)
          .map((board) => (
            <BoardCard key={board.id} name={board.name} board={board} />
          ))}
      </div>
      <h3 className=" text-light mb-4">
        {" "}
        <i className="fa-regular fa-user"></i> &nbsp; Personal Boards
      </h3>
      <div className="not-starred-boards d-flex mx-auto mt-2 flex-wrap justify-content-start">
        {allBoards
          .filter((board) => !board.starred)
          .map((board) => (
            <BoardCard key={board.id} board={board} />
          ))}
        <Card className="mb-5 mx-3 fs-5 text-dark board-card d-flex align-items-center justify-content-center h-50 py-4 px-2">
          <AddNew
            parentId={null}
            parentName={null}
            itemName="Board"
            onItemAdded={addNewBoard}
          />
        </Card>
      </div>
    </div>
  );
};

export default Home;
