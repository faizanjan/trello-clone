import Modal from "react-bootstrap/Modal";
import { useState, useEffect } from "react";
import { useErrorUpdate } from "../contexts/ErrorContext";
import Checklist from "./Checklist";
import AddNew from "./AddNewItem";
import { getAllChildren } from "../modules/TrelloAPI";

function CardModal(props) {
  let { card } = props;

  const [isLoading, setIsLoading] = useState(true);
  const [allChecklists, setAllChecklists] = useState(null);
  const [areChecklistsUpdated, setAreChecklistssUpdated] = useState(false);

  const setError = useErrorUpdate();
  useEffect(() => {
    getAllChildren("card", "checklist", card.id)
      .then((data) => {
        setAllChecklists(data.data);
        setIsLoading(false);
        setAreChecklistssUpdated(false);
      })
      .catch((error) => {
        setError(error.message);
        setIsLoading(false);
      });
  }, [areChecklistsUpdated]);

  const addNewChecklist = (checklist) => {
    setAllChecklists((prevState) => [...prevState, checklist]);
  };

  let onItemRemoved = () => {
    setAreChecklistssUpdated(true);
  };

  if (isLoading) return;

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      style={{ color: "rgb(51,68,98)" }}
    >
      <Modal.Header
        className="d-flex flex-row align-items-start"
        style={{ background: "rgb(240,241,244)" }}
      >
        <Modal.Title id="contained-modal-title-vcenter">
          <i className="fa-solid fa-credit-card me-1"></i>
          &nbsp;
          {"  " + card.name}
          <p className="text-muted ms-5" style={{ fontSize: "0.8rem" }}>
            in <strong>{props.listname}</strong> List
          </p>
        </Modal.Title>
        <i className="fa-solid fa-multiply fs-4" onClick={props.onHide}></i>
      </Modal.Header>
      <Modal.Body>
        {allChecklists.length !== 0
          ? allChecklists.map((checklist) => (
              <Checklist
                key={checklist.id}
                checklist={checklist}
                onChecklistDelete={onItemRemoved}
                cardId={card.id}
              />
            ))
          : "No Checklists"}
        <AddNew
          parentId={card.id}
          parentName="Card"
          itemName="Checklist"
          onItemAdded={addNewChecklist}
        />
      </Modal.Body>
    </Modal>
  );
}

export default CardModal;
