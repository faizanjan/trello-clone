import ProgressBar from 'react-bootstrap/ProgressBar';

function ChecklistProgress({percentage}) {
  return <ProgressBar className='mt-3' now={percentage} label={`${percentage}%`} visuallyHidden />;
}

export default ChecklistProgress;