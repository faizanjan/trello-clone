import { useState, useEffect } from "react";
import { useErrorUpdate } from "../contexts/ErrorContext";
import AddNew from "./AddNewItem";
import CheckItems from "./CheckItem";
import ChecklistProgress from "./ProgressBar";
import { deleteItem, getAllChildren } from "../modules/TrelloAPI";

let Checklist = ({ checklist, onChecklistDelete, cardId }) => {
  const [isLoading, setIsLoading] = useState(true);
  const [allCheckItems, setAllCheckItems] = useState(null);
  const [areCheckItemsUpdated, setAreCheckItemsUpdated] = useState(false);
  const [completedCount, setCompletedCount] = useState(0);

  const setError = useErrorUpdate();
  useEffect(() => {
    getAllChildren("checklist", "checkitem", checklist.id)
      .then((data) => {
        setAllCheckItems(data.data);
        setAreCheckItemsUpdated(false);
        let completedItems = data.data.reduce((acc, checkitem) => {
          if (checkitem.state === "complete") acc++;
          return acc;
        }, 0);
        setCompletedCount(completedItems);
        setIsLoading(false);
      })
      .catch((error) => {
        setError(error.message);
        setIsLoading(false);
      });
  }, [areCheckItemsUpdated]);

  let onItemUpdated = () => {
    setAreCheckItemsUpdated(true);
  };

  const addNewCheckItem = (checkitem) => {
    setAllCheckItems((prevState) => [...prevState, checkitem]);
  };

  let deleteChecklist = () => {
    deleteItem("checklist", checklist.id)
      .then(() => {
        onChecklistDelete();
      })
      .catch((error) => {
        console.error("Error fetching country data:", error);
      });
  };

  if (isLoading) return;
  return (
    <div
      className="checklist border-secondary shadow rounded-2 p-3 mb-4 mx-3"
      style={{ background: "rgb(240,241,244)" }}
    >
      <div className="checklist-header d-flex flex-row justify-content-between align-items-center border-bottom pb-2">
        <span className="py-2 d-flex align-items-center">
          <i className="fa-solid fa-list-check me-2"></i> &nbsp;
          <h5 className="d-inline my-auto">{checklist.name}</h5>
        </span>
        <button
          className="btn btn-light border"
          style={{ color: "rgb(51,68,98)" }}
          onClick={deleteChecklist}
        >
          Delete
        </button>
      </div>
      {allCheckItems.length !== 0 && (
        <ChecklistProgress
          percentage={(completedCount * 100) / allCheckItems.length}
        />
      )}
      <div className="checklist-body mt-3">
        {allCheckItems.length !== 0
          ? allCheckItems.map((checkitem) => (
              <CheckItems
                key={checkitem.id}
                checkitem={checkitem}
                checkListId={checklist.id}
                onCheckItemUpdate={onItemUpdated}
                cardId={cardId}
              />
            ))
          : "No CheckItems"}
        <AddNew
          parentId={checklist.id}
          parentName="checklist"
          itemName="CheckItem"
          onItemAdded={addNewCheckItem}
        />
      </div>
    </div>
  );
};

export default Checklist;
