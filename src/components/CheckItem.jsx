import { useErrorUpdate } from "../contexts/ErrorContext";
import { useContext, useState } from "react";
import { UpdateListContext } from "./List";
import { toggleCheckItem, deleteCheckitem } from "../modules/TrelloAPI";

let CheckItems = ({ checkitem, onCheckItemUpdate, checkListId, cardId }) => {
  let [showDeleteBtn, setDeleteBtn] = useState(false);
  let reRenderList = useContext(UpdateListContext);

  const setError = useErrorUpdate();
  let deleteCheckItem = () => {
    deleteCheckitem(checkListId, checkitem.id)
      .then((res) => {
        reRenderList();
        onCheckItemUpdate();
      })
      .catch((error) => {
        setError(error.message);
      });
  };
  1;
  let handleToggle = () => {
    toggleCheckItem(cardId, checkitem)
      .then(() => {
        onCheckItemUpdate();
      })
      .catch((error) => {
        setError(error.message);
      });
  };

  return (
    <div
      className="checkitem d-flex flex-row justify-content-between align-items-center px-5 py-2"
      style={{
        background: `${showDeleteBtn ? "rgb(226,228,234)" : "inherit"}`,
      }}
      onMouseOver={() => {
        setDeleteBtn(true);
      }}
      onMouseOut={() => {
        setDeleteBtn(false);
      }}
    >
      <span
        className="d-flex flex-row align-items-center"
        style={{ width: "90%" }}
      >
        <input
          type="checkbox"
          id={checkitem.id}
          defaultChecked={checkitem.state === "complete"}
          onClick={handleToggle}
          style={{
            accentColor: "rgb(51,68,98)",
            width: "1.5rem",
            height: "1.1rem",
          }}
        />
        <label
          htmlFor={checkitem.id}
          className="ms-3 w-100"
          style={{
            textDecoration: `${
              checkitem.state === "complete" ? "line-through" : ""
            }`,
            fontSize: "1.1rem",
          }}
        >
          {checkitem.name}
        </label>
      </span>
      {showDeleteBtn && (
        <i
          className="fa-solid fa-trash-can"
          onClick={() => {
            deleteCheckItem();
          }}
        ></i>
      )}
    </div>
  );
};

export default CheckItems;
