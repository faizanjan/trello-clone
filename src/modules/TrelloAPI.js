import axios from "axios";
const key = "3b5a3a5253711b4e832f76da6e056888";
const token =
  "ATTA84916fc0212d2e3f1df20309c3ae55f753c7540a999df9607d7a523da4465f670505DC7D";

export async function getAllBoards() {
  const allBoardsUrl = `https://api.trello.com/1/members/me/boards?&key=${key}&token=${token}`;
  let boards = await axios.get(allBoardsUrl);
  return boards;
}

export async function getBoardDetails(idBoard) {
  const boardURL = `https://api.trello.com/1/boards/${idBoard}?key=${key}&token=${token}`;
  let boardDetails = await axios.get(boardURL);
  return boardDetails;
}

export async function deleteList(list) {
  let archiveUrl = `https://api.trello.com/1/lists/${list.id}/closed?value=true&key=${key}&token=${token}`;
  return await axios({
    method: "put",
    url: archiveUrl,
    data: {
      ...list,
      closed: true,
    },
  });
}

export async function deleteCheckitem(checkListId, checkItemId) {
  let deleteCheckItemUrl = `https://api.trello.com/1/checklists/${checkListId}/checkItems/${checkItemId}?key=${key}&token=${token}`;
  axios.delete(deleteCheckItemUrl);
}

export async function createCheckitem(
  checkListId,
  checkitemName,
  checkitemState
) {
  return await axios.post(
    `https://api.trello.com/1/checklists/${checkListId}/checkItems?name=${checkitemName}&checked=${
      checkitemState === "incomplete"
    }&key=${key}&token=${token}`
  );
}

export async function toggleCheckItem(cardId, checkitem) {
  let toggleUrl = `https://api.trello.com/1/cards/${cardId}/checkItem/${checkitem.id}?key=${key}&token=${token}`;
  return await axios({
    method: "put",
    url: toggleUrl,
    data: {
      ...checkitem,
      state: checkitem.state === "complete" ? "incomplete" : "complete",
    },
  });
}

// GENERIC FUNCTIONS

export async function getAllChildren(parentName, itemName, idParent) {
  const childrenUrl = `https://api.trello.com/1/${parentName}s/${idParent}/${itemName}s?key=${key}&token=${token}`;
  let lists = await axios.get(childrenUrl);
  return lists;
}

export async function deleteItem(itemName, idItem) {
  const deleteItemUrl = `https://api.trello.com/1/${itemName}/${idItem}?key=${key}&token=${token}`;
  return await axios.delete(deleteItemUrl);
}

export async function createItem(itemName, newItem, parentName, parentId) {
  let url = `https://api.trello.com/1/${itemName}s?${
    itemName === "List" || itemName === "Board" ? "name=" + newItem : ""
  }${
    itemName !== "Board" ? `id${parentName}=${parentId}` : ""
  }&key=${key}&token=${token}`;

  let url_for_CreateCheckItems = `https://api.trello.com/1/checklists/${parentId}/checkItems?name=${itemName}&key=${key}&token=${token}`;

  return await axios.post(
    itemName === "CheckItem" ? url_for_CreateCheckItems : url,
    {
      name: newItem,
      [`id${parentName}`]: parentId,
      prefs_background: itemName === "Board" ? "sky" : "",
    }
  );
}
