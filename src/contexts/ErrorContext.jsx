import { createContext, useContext, useEffect, useState } from "react";

const ErrorContext = createContext();
export const useErrorUpdate = () => useContext(ErrorContext);

export default ({ children }) => {
  const [error, setError] = useState(null);

  return (
    <ErrorContext.Provider value={setError}>
      {!error ? (
        children
      ) : (
        <div className="error-container">
          <i
            className="fa-solid fa-multiply fs-1"
            style={{
              position: "absolute",
              right: "20px",
              top: "10px",
              color: "rgb(114,28,36)",
            }}
            onClick={() => setError(null)}
          ></i>
          <div className="errorIcon"></div>
          <p className="errorMessage">{error}</p>
        </div>
      )}
    </ErrorContext.Provider>
  );
};
