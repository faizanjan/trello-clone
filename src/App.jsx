import "bootstrap/dist/css/bootstrap.min.css";
import { Routes, Route } from "react-router-dom";

import ErrorProvider from "./contexts/ErrorContext";
import Nav from "./components/Nav";
import Home from "./components/Home";
import Board from "./components/Board";

function App() {
  return (
      <ErrorProvider>
        <Nav />
        <Routes>
          <Route element={<Home />} path="/boards" />
          <Route element={<Board />} path="/boards/:boardId" />
          <Route element={<Home/>} path=""/>
        </Routes>
      </ErrorProvider>
  );
}

export default App;
